import { getSwimmingFacilityByName } from '../src/models/SwimmingFacility';

describe('Getting swimming facility by name', () => {
    test('it should return swimming facility object', () => {
        const input = 'Kúpalisko Rosnička';
        const output = {id: 1, name: 'Kúpalisko Rosnička'};

        return expect(getSwimmingFacilityByName(input)).resolves.toEqual(output);
    });

    test('it should return no swimming facility found error', () => {
        const input = 'Magistrát BA';
        const error = new Error('No swimming facility found with provided name: ' + input);

        return expect(getSwimmingFacilityByName(input)).rejects.toEqual(error);
    });
});