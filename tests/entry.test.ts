import * as entry from '../src/controllers/entry';
import { SwimmingFacility } from '../src/models/SwimmingFacility';
import { Ticket } from '../src/models/Ticket';
import { User } from '../src/models/User';

interface EntryData {
    user: User,
    ticket: Ticket,
    swimmingFacility: SwimmingFacility
}

describe('Handling entry', () => {
    it('should return response object with allowed entry', () => {
        // let spy = jest.spyOn(TicketLog, 'logOnGate');
        // spy.mockReturnValue(null);

        const user = {
            id: 1,
            first_name: 'John',
            last_name: 'Doe'
        }
        const swimmingFacility = {
            id: 1,
            name: 'Kúpalisko Rosnička'
        }
        const ticket = {
            id: 1,
            user_id: 1,
            entries_remaining: 9,
            type: 'TEN_ENTRIES'
        }

        const isAllowed = true;
        const entryData: EntryData = {user, swimmingFacility, ticket};

        const output = {
            resData: {
                ticket,
                userName: 'John Doe',
                entryAllowed: true
            },
            statusCode: 201
        }

        expect(entry.handleEntry(isAllowed, entryData)).toEqual(output);
    });
});