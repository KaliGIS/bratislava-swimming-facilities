import * as ticketModel from '../src/models/Ticket';

describe('Getting ticket by id', () => {
    test('it should return ticket object', () => {
        const input = 1;
        const output = {id: 1, user_id: 1, entries_remaining: 10, type: 'TEN_ENTRIES'}

        return expect(ticketModel.getTicketById(input)).resolves.toEqual(output);
    });

    test('it should return no ticket found error', () => {
        const input = 100;
        const output = new Error('No ticket found with provided id: ' + input);

        return expect(ticketModel.getTicketById(input)).rejects.toEqual(output);
    });
});

describe('Subtracting one remaining entry', () => {
    test('it should return ticket object with remaining entries lowered by one', () => {
        const input = 1
        const output = {id: 1, user_id: 1, entries_remaining: 9, type: 'TEN_ENTRIES'}

        return expect(ticketModel.subtractOneRemainingEntry(input)).resolves.toEqual(output);
    });

    test('it should return error with an information about zero affected rows by update', () => {
        const input = 100
        const output = new Error('No ticket was affected by update. Does the ticket id exist? ticket_id: ' + input.toString());

        return expect(ticketModel.subtractOneRemainingEntry(input)).rejects.toEqual(output);
    });
});