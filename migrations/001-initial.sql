CREATE TABLE users (
    id INTEGER PRIMARY KEY,
    first_name TEXT,
    last_name TEXT
);

CREATE TABLE tickets (
    id INTEGER PRIMARY KEY,
    user_id INTEGER,
    entries_remaining INTEGER NOT NULL,
    type TEXT CHECK (type IN ('SINGLE', 'TEN_ENTRIES', 'SEASON')) NOT NULL,
    FOREIGN KEY (user_id) REFERENCES users(id) ON UPDATE CASCADE ON DELETE CASCADE
);

CREATE TABLE swimming_facilities (
    id INTEGER PRIMARY KEY,
    name TEXT
);

CREATE TABLE ticket_logs (
    id INTEGER PRIMARY KEY,
    ticket_id INTEGER,
    swimming_facility_id INTEGER,
    direction TEXT CHECK (direction IN ('IN', 'OUT')) NOT NULL,
    check_time TEXT NOT NULL DEFAULT (datetime('now')),
    FOREIGN KEY (ticket_id) REFERENCES tickets(id) ON UPDATE CASCADE ON DELETE SET NULL,
    FOREIGN KEY (swimming_facility_id) REFERENCES swimming_facilities(id) ON UPDATE CASCADE ON DELETE SET NULL
);