INSERT INTO users(id, first_name, last_name) VALUES (1, 'John', 'Doe');
INSERT INTO users(id, first_name, last_name) VALUES (2, 'Jane', 'Doe');
INSERT INTO users(id, first_name, last_name) VALUES (3, 'Simon', 'Fullop');

INSERT INTO tickets(id, user_id, entries_remaining, type) VALUES (1, 1, 10, 'TEN_ENTRIES');
INSERT INTO tickets(id, user_id, entries_remaining, type) VALUES (2, 2, 1, 'SINGLE');
INSERT INTO tickets(id, user_id, entries_remaining, type) VALUES (3, 3, -1, 'SEASON');

INSERT INTO swimming_facilities(id, name) VALUES (1, 'Kúpalisko Rosnička');
INSERT INTO swimming_facilities(id, name) VALUES (2, 'Kúpalisko Lamač');
INSERT INTO swimming_facilities(id, name) VALUES (3, 'Kúpalisko Tehelné pole');
INSERT INTO swimming_facilities(id, name) VALUES (4, 'Kúpalisko Delfín');
INSERT INTO swimming_facilities(id, name) VALUES (5, 'Areál zdravia Zlaté piesky');
INSERT INTO swimming_facilities(id, name) VALUES (6, 'Kúpalisko Krasňany');
INSERT INTO swimming_facilities(id, name) VALUES (7, 'Kúpalisko Rača');