import db from '../db/sqlite';

export interface Ticket {
    id: number,
    user_id: number,
    entries_remaining: number,
    type: string
}

export function getTicketById(ticketId: number): Promise<Ticket> {
    return new Promise((resolve, reject) => {
        const query: string = 'SELECT * FROM tickets WHERE id = ?';
        const params: any[] = [ticketId];

        db.get(query, params, (err, row) => {
            if (err) {
                return reject(err);
            }

            if (row) {
                return resolve(row);
            }

            return reject(new Error('No ticket found with provided id: ' + ticketId));
        });
    });
}

export function subtractOneRemainingEntry(ticketId: number): Promise<Ticket> {
    return new Promise((resolve, reject) => {
        const query: string = `
            UPDATE tickets SET entries_remaining =
                CASE
                    WHEN entries_remaining < 0
                        THEN -1
                        ELSE entries_remaining - 1
                END
                WHERE id = ?
        `;
        const params: any[] = [ticketId];

        db.run(query, params, function(err) {
            if (err) {
                return reject(err);
            }
            if (this.changes === 0) {
                return reject(new Error('No ticket was affected by update. Does the ticket id exist? ticket_id: ' + ticketId.toString()));
            }

            return resolve(getTicketById(ticketId));
        });
    });
}