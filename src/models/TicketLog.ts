import db from '../db/sqlite';
import moment from 'moment';
import { Constants as Con } from '../constants';

interface TicketLog {
    id: number,
    ticket_id: number,
    swimming_facility_id: number,
    direction: string,
    check_time: string,
}

type directionString = 'IN' | 'OUT';

function getTicketLogById(ticketLogid: number): Promise<TicketLog> {
    return new Promise((resolve, reject) => {
        const query = `
            SELECT * FROM ticket_logs WHERE id = ?
        `;
        const params = [ticketLogid];

        db.get(query, params, (err, row) => {
            if (err) {
                reject(err);
            }

            if (row) {
                resolve(row);
            }

            reject(new Error('No ticket log found by provided id: ' + ticketLogid.toString()));
        });
    });
}

export function logOnGate(ticketId: number, swimmingFacilityId: number, direction: directionString): Promise<TicketLog> {
    const checkTime = moment().format(Con.DATETIME_FORMAT);

    return new Promise((resolve, reject) => {
        const query: string = `
            INSERT INTO ticket_logs(ticket_id, swimming_facility_id, direction, check_time) VALUES (?, ?, ?, ?)
        `;
        const params: any[] = [ticketId, swimmingFacilityId, direction, checkTime];

        db.run(query, params, function (err) {
            if (err) {
                reject(err);
            }

            resolve(getTicketLogById(this.lastID));
        });
    });
}

export function getAll(): Promise<TicketLog[]> {
    return new Promise((resolve, reject) => {
        const query = `SELECT * FROM ticket_logs`;

        db.all(query, (err, rows) => {
            if (err) {
                reject(err);
            }

            resolve(rows);
        });
    });
}

export function getSortedLogsOfTicketByTicketSwimmingFacilityAndDirection(
        ticketId: number, swimmingFacilityId: number, direction: string): Promise<TicketLog[]> {
    return new Promise((resolve, reject) => {
        const query = `
            SELECT * FROM ticket_logs WHERE ticket_id = ? AND swimming_facility_id = ? AND direction = ?
                ORDER BY check_time DESC
        `;
        const params = [ticketId, swimmingFacilityId, direction];

        db.all(query, params, (err, rows) => {
            if (err) {
                reject(err);
            }

            resolve(rows);
        });
    });
}