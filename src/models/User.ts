import db from '../db/sqlite';

export interface User {
    id: number,
    first_name: string,
    last_name: string
}

export function getUserById(userId: number): Promise<User> {
    return new Promise((resolve, reject) => {
        const query: string = 'SELECT * FROM users WHERE id = ?';
        const params: any[] = [userId];
        db.get(query, params, (err, row) => {
            if (err) {
                reject(err);
            }
            if (row === undefined) {
                reject(new Error('No user found with provided id: ' + userId.toString()));
            }
            resolve(row);
        });
    });
}