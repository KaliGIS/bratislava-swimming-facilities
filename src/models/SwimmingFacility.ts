import db from '../db/sqlite';

export interface SwimmingFacility {
    id: number,
    name: string,
}

export function getSwimmingFacilityByName(name: string): Promise<SwimmingFacility> {
    return new Promise((resolve, reject) => {
        const query = `SELECT * FROM swimming_facilities WHERE name = ?`;
        const params = [name];

        db.get(query, params, (err, row) => {
            if (err) {
                reject(err);
            }
            if (row) {
                resolve(row);
            }

            reject(new Error('No swimming facility found with provided name: ' + name));
        });
    });
}