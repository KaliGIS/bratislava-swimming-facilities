import sqlite3 from 'sqlite3';
import fs from 'fs';
import path from 'path';
import util from 'util';


const db = new sqlite3.Database(':memory:', err => {
    if (err) {
        return console.error(err.message);
    }
    console.log('Connected to the in-memory SQlite database.');
});

const migrationsPath = path.join(__dirname, '..', '..', 'migrations');
const sqlFiles = fs.readdirSync(migrationsPath);
const sqlArr: string[] = [];
sqlFiles.forEach(file => {
    const fileSrc = path.join(migrationsPath, file);
    const tempArr = fs.readFileSync(fileSrc).toString().split(';');
    tempArr.forEach(item => {
        sqlArr.push(item);
    })
});

db.serialize(() => {
    console.log(sqlArr.length);
    sqlArr.forEach(query => {
        console.log(`Processing query: ${query}`);
        db.run(query, (err) => {
            if (err) {
                console.log('ERROR occured near query: ' + query);
                console.log(err);
            }
        });
    });
});

export default db;