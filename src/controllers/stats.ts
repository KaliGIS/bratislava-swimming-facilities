import express from 'express';
import { Constants as Con } from '../constants';
import { getAll } from '../models/TicketLog';

export function getTicketLogs(req: express.Request, res: express.Response) {
    getAll()
    .then(ticketLogs => {
        res.send(ticketLogs);
    })
    .catch(err => res.status(400).send(err));
}
