import express from 'express';
import { validationResult } from 'express-validator';
import { Constants as Con } from '../constants';
import { getTicketById, subtractOneRemainingEntry } from '../models/Ticket';
import { getUserById } from '../models/User';
import { logOnGate } from '../models/TicketLog';
import { getSwimmingFacilityByName } from '../models/SwimmingFacility';

export function postExit(req: express.Request, res: express.Response) {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        return res.status(400).json({errors: errors.array()});
    }

    Promise.all([
        getTicketById(req.body.ticket_id),
        getSwimmingFacilityByName(req.body.swimming_facility)
    ])
    .then(results => {
        const ticket = results[0];
        const swimmingFacility = results[1];

        logOnGate(ticket.id, swimmingFacility.id, Con.DIRECTION_OUT);

        return getUserById(ticket.user_id)
        .then(user => {
            const userName: string = user.first_name + ' ' + user.last_name;
            const resObj = {
                ticket,
                userName,
            }

            return res.status(201).json(resObj);
        });
    })
    .catch(err => res.status(500).send(err.toString()));
}