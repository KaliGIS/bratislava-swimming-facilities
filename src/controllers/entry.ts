import express from 'express';
import { validationResult } from 'express-validator';
import moment from 'moment';
import { Constants as Con } from '../constants';
import { getTicketById, subtractOneRemainingEntry, Ticket } from '../models/Ticket';
import { getUserById, User } from '../models/User';
import { logOnGate, getSortedLogsOfTicketByTicketSwimmingFacilityAndDirection } from '../models/TicketLog';
import { getSwimmingFacilityByName, SwimmingFacility } from '../models/SwimmingFacility';

interface EntryData {
    user: User,
    ticket: Ticket,
    swimmingFacility: SwimmingFacility
}

interface ResposneData {
    ticket: Ticket,
    userName: string,
    entryAllowed: boolean
}

export function postEntry(req: express.Request, res: express.Response) {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        return res.status(400).json({errors: errors.array()});
    }

    Promise.all([
        getTicketById(req.body.ticket_id),
        getSwimmingFacilityByName(req.body.swimming_facility),
    ])
    .then(results => {
        const ticket = results[0];
        const swimmingFacility = results[1];

        return getUserById(ticket.user_id)
        .then(user => {
            return checkIfFirstEntryToday(ticket.id, swimmingFacility.id, Con.DIRECTION_IN)
            .then(isFirstEntryToday => {
                if (isFirstEntryToday) {
                    if (ticket.entries_remaining > 0 || ticket.type === Con.TICKET_TYPE_SEASON) { // entry allowed
                        return subtractOneRemainingEntry(ticket.id)
                        .then(modifiedTicket => {
                            const responseObj = handleEntry(true, {user, ticket: modifiedTicket, swimmingFacility});
                            return res.status(responseObj.statusCode).json(responseObj.resData);
                        });
                    } else { // entry disallowed
                        const responseObj = handleEntry(false, {user, ticket, swimmingFacility});
                        return res.status(responseObj.statusCode).json(responseObj.resData);
                    }
                } else { // entry allowed
                    const responseObj = handleEntry(true, {user, ticket, swimmingFacility});
                    return res.status(responseObj.statusCode).json(responseObj.resData);
                }
            });
        });
    })
    .catch(err => {
        res.status(500).send(err.toString());
    });
}

export function handleEntry(isAllowed: boolean, entryData: EntryData): {resData: ResposneData, statusCode: number} {
    const userName: string = entryData.user.first_name + ' ' + entryData.user.last_name;
    let statusCode: number = 200;

    if (isAllowed) {
        logOnGate(entryData.ticket.id, entryData.swimmingFacility.id, Con.DIRECTION_IN);
        statusCode = 201;
    }

    const resData = {
        ticket: entryData.ticket,
        userName,
        entryAllowed: isAllowed
    }

    return { resData, statusCode };
}

function checkIfFirstEntryToday(ticketId: number, swimmingFacilityId: number, direction: string): Promise<boolean> {
    return getSortedLogsOfTicketByTicketSwimmingFacilityAndDirection(ticketId, swimmingFacilityId, direction)
    .then(ticketLogs => {
        if (ticketLogs.length > 0) {
            const lastCheckInTime = moment(ticketLogs[0].check_time, Con.DATETIME_FORMAT);
            const todayMidnight = moment().startOf('day');
            const difference = lastCheckInTime.diff(todayMidnight);
            console.log(`last check in time: ${moment(lastCheckInTime).format(Con.DATETIME_FORMAT)}`);
            console.log(`last check in time (ms): ${moment(lastCheckInTime).valueOf()}`);
            console.log(`Difference between checkIns in ms: ${difference}`);

            if (difference <= Con.DAY_MILISECONDS) {
                return false;
            }
        }

        return true;
    });
}