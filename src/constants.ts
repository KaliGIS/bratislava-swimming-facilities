export class Constants {
    static readonly PORT = 3000;

    static readonly TICKET_TYPE_SEASON = 'SEASON';
    static readonly TICKET_TYPE_TEN_ENTRIES = 'TEN_ENTRIES';
    static readonly TICKET_TYPE_SINGLE = 'SINGLE';

    static readonly DATETIME_FORMAT = 'YYYY-MM-DD HH:mm:ss';

    static readonly DIRECTION_IN = 'IN';
    static readonly DIRECTION_OUT = 'OUT';

    static readonly DAY_MILISECONDS = 86400000;
};