import express from 'express';
import { body } from 'express-validator';
import { Constants as Con } from './constants';
import db from './db/sqlite';
import { postEntry } from './controllers/entry';
import { postExit } from './controllers/exit';
import { getTicketLogs } from './controllers/stats';

const app = express();

app.use(express.json());

app.post('/entry',
    body('swimming_facility').not().isEmpty(),
    body('ticket_id').isNumeric().not().isEmpty(),
    postEntry);

app.post('/exit',
    body('swimming_facility').not().isEmpty(),
    body('ticket_id').isNumeric().not().isEmpty(),
    postExit);

app.get('/users', (req, res) => {
    db.all('SELECT * FROM users', (err, users) => {
        res.send(users);
    })
});

app.get('/ticketlogs', getTicketLogs);

const server = app.listen(Con.PORT, () => {
    console.log(`BA backend challenge app is listening on the port ${Con.PORT}`);
});

process.on('SIGINT', () => {
    server.close(err => {
        console.log('Server is closing connetion');
        db.close();
    });
});

