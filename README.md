# Bratislava swimming facilities #

Tento projekt predstavuje backendovú časť aplikácie simulujúcu správu lístkov, vstupov a výstupov na bratislavské kúpaliská.

## Špecifikácia aplikácie ##

Aplikácia je postavená na NodeJS Express JS serveri s využitím programovacieho jazyka TypeScript. Na ukladanie dát je implementovaná SQLite in memory databáza, ktorá sa pri každom spustení servera nanovo inicializuje a napĺňa testovacími dátami.

### Údajový model ###

Databáza pozostáva z tabuliek:

* users - tabuľka používateľov so stĺpcami
    * id - identifikátor používateľa
    * first_name - krstné meno
    * last_name - priezvisko
* tickets - tabuľka, ktorá v sebe ukladá vstupenky na kúpaliská so stĺpcami
    * id - identifikátor lístka
    * user_id - cudzí kľúč na pole id v tabuľke users
    * entries_remaining - počet zostávajúcich vstupov na kúpalisko
    * type - druh lístka (jednorazová vstupenka: SINGLE, 10-vstupový lístok: TEN_ENTRIES, celosezónna permanentka: SEASON)
* swimming_facilities - tabuľka s bratislavskými kúpaliskami so stĺpcami
    * id - identifikátor kúpaliska
    * name - názov kúpaliska
* ticket_logs - logovacia tabuľka, do ktorej sa zapisuje všetky príchody a odchody z kúpalísk (aby bolo možné na konci leta robiť štatistiky) so stĺpcami
    * id - identifikátor záznamu
    * ticket_id - cudzí kľúč na pole id v tabuľke tickets
    * swimming_facility_id - cudzí kľúč na pole id v tabuľke swimming_facilities
    * direction - smer rekreanta - vstup: IN alebo výstup: OUT z kúpaliska
    * check_time - dátum a čas, kedy bol rekreantovi naskenovaný lístok na bráne kúpaliska

Databáza je naplnená 3 používateľmi (John Doe, Jane Doe, Simon Fullop) a 3 lístkami. Jednému používateľovi je priradený jeden typ lístka. Lístok s id 1 predstavuje 10-vstupovú permanentku (John Doe). Lístok s id 2 predstavuje jednovstupový lístok (Jane Doe). Lístok s id 3 predstavuje celosezónnu permanentku (Simon Fullop). V tabuľke swimming_facilities sú dáta bratislavských kúpalísk (v správe STARZu). SQL súbor s testovacími dátami: /migrations/002-insertDefaultData.sql.

## Postup na lokálne otestovanie ##

0. Postup bol otestovaný na Windows 10 Pro. Je potrebné mať nainštalovaný NodeJS (testované na verzii v16.13.0).
1. Stiahnutie / clone tohto repozitára.
2. Otvoríme príkazový riadok a nastavíme sa do priečinka so stiahnutým kódom.
3. Nainštalujeme potrebné dependencie:
    
    `npm install`

4. Spustíme si lokálny server:

    `npm start`

5. Endpointy testujeme, napr. pomocou aplikácie Postman:

    `localhost:3000/entry` - metóda POST

    `localhost:3000/exit` - metóda POST

    `localhost:3000/ticketlogs` - metóda GET

### POST /entry ###
Request body má obsahovať JSON s dvoma poliami - swimming_facility, ticket_id. Implicitne sa očakáva, že každé skenovacie zariadenie na bráne kúpaliska odošle meno daného kúpaliska ako atribút swimming_facility a odčítaním z QR kódu zistí identifikátor lístka, ktorý pošle v atribúte ticket_id.
Príklad tela requestu:

    {
        "swimming_facility": "Kúpalisko Rosnička",
        "ticket_id": 1
    }

Možno interpretovať ako: lístok s identifikátorom 1 (10-vstupová permanentka) vstupuje na Kúpalisko Rosnička.

Response body predstavuje JSON v tvare, napr.:

    {
        "ticket": {
            "id": 1,
            "user_id": 1,
            "entries_remaining": 9,
            "type": "TEN_ENTRIES"
        },
        "userName": "John Doe",
        "entryAllowed": true
    }
kde ticket predstavuje údaje lístka, userName predstavuje meno a priezvisko rekreanta. Rozhodujúcou pre vstup je hodnota v poli entryAllowed. Pre `true` rekreant môže vstúpiť na kúpalisko. Pre `false` rekreant nemôže vstúpiť na kúpalisko. Z ticketu môžeme vyčítať počet zostávajúcich vstupov entries_remaining a typ lístka (type). 

Pre celosezónnu vstupenku (type = SEASON) je vyhradená hodnota entries_remaining = -1 a táto hodnota sa nemení. Pri 10-vstupovom alebo jednovstupovom lístku sa hodnota entries_remaining pri úspešnom POST /entry requeste odčíta o 1. Pokiaľ sa rekreant vráti na to isté kúpalisko v ten istý deň (po zmrzline), hodnota entries_remaining sa pri takomto vstupe neodpočíta.

Údaje o vstupe sa zapisujú do tabuľky ticket_logs. Táto tabuľka je dôležitá pri vyhodnocovaní, či sa rekreant vrátil na to isté kúpalisko v tom istom dni.

### POST /exit ###
Request body má obsahovať tie isté dáta ako request body na POST /entry.

Response body predstavuje JSON v tvare, napr.:

    {
        "ticket": {
            "id": 1,
            "user_id": 1,
            "entries_remaining": 9,
            "type": "TEN_ENTRIES"
        },
        "userName": "John Doe"
    }

V tomto prípade response body nenesie informáciu entryAllowed.

Údaje o výstupe z kúpaliska sa zapisujú do tabuľky ticket_logs.

### GET /ticketlogs ###
Pri tomto requeste sa neposiela body.

Response body obsahuje informácie o všetkých vstupoch a výstupoch z kúpalísk pre všetky registrované lístky. Z týchto údajov je na konci sezóny možné robiť štatistiky.

    [
        {
            "id": 1,
            "ticket_id": 1,
            "swimming_facility_id": 1,
            "direction": "IN",
            "check_time": "2021-11-07 20:48:20"
        },
        {
            "id": 2,
            "ticket_id": 1,
            "swimming_facility_id": 1,
            "direction": "OUT",
            "check_time": "2021-11-07 20:48:26"
        },
        {
            "id": 3,
            "ticket_id": 1,
            "swimming_facility_id": 1,
            "direction": "IN",
            "check_time": "2021-11-07 20:48:30"
        },
        {
            "id": 4,
            "ticket_id": 1,
            "swimming_facility_id": 1,
            "direction": "OUT",
            "check_time": "2021-11-07 20:48:34"
        }
    ]

## Unit testy ##
Projekt obsahuje pár demonštračných unit testov (vzhľadom na krátkosť času ich nebolo vyvinutých viac). Testy sú písané vo frameworku Jest. Spustenie unit testov realizujeme príkazom `npm test`.